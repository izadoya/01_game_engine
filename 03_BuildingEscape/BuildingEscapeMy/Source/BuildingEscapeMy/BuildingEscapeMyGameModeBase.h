// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BuildingEscapeMyGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BUILDINGESCAPEMY_API ABuildingEscapeMyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
